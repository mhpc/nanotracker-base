# NanoTracker Base

This is the base library which makes possible to make the core abstracted from
OpenCV. With this abstraction, it is possible to use several back-ends and 
decouple the architecture from direct dependencies.

Inside this library, you can find:

* Image representation type
* Video I/O
* Image I/O

At the moment, this requires:

* OpenCV 4.x
* Meson 0.50

## Construction

To build the project:

```bash
meson buildir
ninja -C builddir
```

The library will be compiled inside.

For running the tests:

```bash
ninja test -C builddir
```

### Installation

For installing the library:

Default location:

```bash
meson install -C buildir
```

Custom location:

```bash
DESTDIR=<custom_path> meson install -C buildir
```

Author: Luis G. Leon Vega <luis@luisleon.me>
Version: 0.1.0
